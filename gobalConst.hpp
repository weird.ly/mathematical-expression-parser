#ifndef GLOBALCONSTANT_H
#define GLOBALCONSTANT_H



enum 
{


// operator
	ADDITION_OPERATOR = '+',
	SUBSTRACTION_OPERATOR = '-',
	MULTIPLICATION_OPERATOR = '*',
	DIVISION_OPERATOR = '/',
	EXPONENTIATION_OPERATOR = '^',



//mathematic functions
	SINUS_FUNCTION = 's',
	COSINUS_FUNCTION = 'c',
	TANGENT_FUNCTION = 't', 
	LN_FUNCTION = 'l', 
	LOG_FUNCTION = 'o',


//useful constant for checking purpose 
	LEFT_PARENTHESIS = '(',
	RIGHT_PARENTHESIS = ')',

	FLOATING_POINT = '.',
	DELIMITERCHAR = ' '
	
};

#endif //GLOBALCONSTANT_H