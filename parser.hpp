#include "Expression.hpp"
#include "Stack.hpp"
#include <stack>
#include <cmath>

#ifndef PARSER_H 
#define PARSER_H

const Expression infixExpressionToPostfixExpression(const Expression &exp);

double calculate(double fValue, double sValue, char op);

double caculate(double value, char func);

double eval(Expression &exp);


double calculate(double fValue, double sValue, char op)
{
    switch(op)
    {
        case ADDITION_OPERATOR:
            return fValue + sValue;
        case SUBSTRACTION_OPERATOR:
            return fValue - sValue;
        case MULTIPLICATION_OPERATOR:
            return fValue * sValue;
        case DIVISION_OPERATOR:
            return fValue / sValue; //fix   throw an error missing 
         case EXPONENTIATION_OPERATOR:
            return  pow(fValue, sValue); 
    }
    return 0;
}

double calculate(double value, char func)
{
    switch(func)
    {
        case SINUS_FUNCTION:
            return sin(value);
        case COSINUS_FUNCTION:
            return cos(value);
        case TANGENT_FUNCTION:
            return tan(value);
        case LN_FUNCTION:
            return log(value);
        case LOG_FUNCTION:
            return log10(value);
    }
    return 0;//appease gcc will always be call with meaningfull func
}



Expression infixExpressionToPostfixExpression(Expression &exp)
{

    std::string thePosfixString;
    Stack operatorStack;


    for(size_t i = 0; i < exp.size(); i++)
    {
        if(exp.at(i).isOperand() || exp.at(i).isFloatingPoint())
            thePosfixString += exp.getCurrentChar();

        else if(exp.at(i).isOperator())
        {

            thePosfixString += DELIMITERCHAR;
            while(!operatorStack.isEmpty() && operatorStack.topHasHigherPrecedenceThan(Operator(exp.getCurrentChar())))
            {
                thePosfixString += operatorStack.top().getItsChar();
                operatorStack.pop();
            }
            operatorStack.push(exp.getOperator());
        }

        else if(exp.at(i).isLeftParenthesis())
            operatorStack.push(exp.getOperator());

        else if(exp.at(i).isRightParenthesis())
        {
            while(!operatorStack.isEmpty() && operatorStack.top().getItsChar() != LEFT_PARENTHESIS)
            {
                thePosfixString += operatorStack.top().getItsChar();
                operatorStack.pop();
            }
            operatorStack.pop();
        }

        //ignore all unknown charater
    }

       
    while(!operatorStack.isEmpty())
    {
        thePosfixString += operatorStack.top().getItsChar();
        operatorStack.pop();
    }


    return Expression(thePosfixString);
}



double eval(Expression &exp)
{
    std::stack<double> _stack;
    double currentValue = 0;
    unsigned int digitCounter = 0, floatingPointPosition = 0;

    for(size_t i = 0; i < exp.size(); i++)
    {
        if(exp.at(i).isOperand())
        {
            currentValue = currentValue * 10 + exp.getOperandValue();
            ++digitCounter;
        }
        else if(exp.at(i).isFloatingPoint())
            floatingPointPosition = digitCounter;
        else if(exp.at(i).isDelimiter())
        {
            if(floatingPointPosition == 0)
                floatingPointPosition = digitCounter;
            double value = currentValue / pow(10, (digitCounter - floatingPointPosition));
            _stack.push(value);

            floatingPointPosition = 0;
            digitCounter = 0;
            currentValue = 0;
        }
        else if(exp.at(i).isOperator() && !exp.isMathematicalFunction())
        {
            double secondValue = _stack.top();
            _stack.pop();
            double firstValue = _stack.top();
            _stack.pop();

            double currentResult = calculate(firstValue, secondValue, exp.getCurrentChar());
            _stack.push(currentResult);
        }
        else if(exp.at(i).isMathematicalFunction())
        {
            double value = _stack.top();
            _stack.pop();

            double currentResult = calculate(value, exp.getCurrentChar());
            _stack.push(currentResult);
        }

    }

    return _stack.top();
}

#endif //PARSER_H