
#include "gobalConst.hpp"
#include <string>

#ifndef UTIL_H
#define UTIL_H

const unsigned int precedence(const char);
const char associativity(const char);

const unsigned int precedence(const char op)
{
    switch(op)
    {
        case ADDITION_OPERATOR:
        case SUBSTRACTION_OPERATOR:
            return 4;
        case MULTIPLICATION_OPERATOR:
        case DIVISION_OPERATOR:
            return 6;
        case EXPONENTIATION_OPERATOR:
            return 8;
        case SINUS_FUNCTION:
        case TANGENT_FUNCTION:
        case COSINUS_FUNCTION:
            return 10;

    }
    return 1; //for left parenthesis
}


const char associativity(const char op)
{
    switch(op)
    {
        case ADDITION_OPERATOR:
        case SUBSTRACTION_OPERATOR:
        case MULTIPLICATION_OPERATOR:
            return 'L'; //left associative 
        case DIVISION_OPERATOR:
        case EXPONENTIATION_OPERATOR:
        case SINUS_FUNCTION:
        case TANGENT_FUNCTION:
        case COSINUS_FUNCTION:
            return 'R'; // right associative

    }
    return 'L'; //default associativity
}


#endif //UTILS_H
